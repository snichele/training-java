# Formation Java

## Introduction

## Exercices

### TP 0.1 - Premier programme en java, mode procédural basique

Ecrivez un programme Java qui va recevoir en paramètres des informations concernants des clients d'une banque pour effectuer des traitements.

Le programme doit recevoir un ou plusieurs clients, et pour chaque client

	- le nom du client ;
	- 3 montants (au format `float`, utilisez le point comme séparateur) correspondants aux soldes des comptes de ce client.

Voici plusieurs exemples d'appels valides

	java NomDeMaClasse John 0.0 0.0 125.85 Bob 1500.0 0.0 25.55
	java NomDeMaClasse "Bob Denart" 100000.0 0.0 0.0

Ce programme doit

- calculer l'avoir global pour chaque client, et l'afficher dans la console
- calculer l'avoir global total de tous les clients, et l'afficher dans la console

		$ java NomDeMaClasse John 0.0 0.0 125.85 Bob 1500.0 0.0 25.55 ↵
		Client : John
		Total avoir = 125.85
		Client : Bob
		Total avoir = 1525.55
		Total tous clients = 1651.4

N'utilisez que des primitives pour résoudre le problème, et ne créez aucune classe (hormis la classe principale du programme) !

Executez votre programme en ligne de commande et vérifiez visuellement que la sortie
console est conforme à ce qui est demandé.

#### Pour vous aider

Utiliser la méthode `Float.valueOf()` pour convertir une  chaine de caractère en nombre a virgule flottante.

#### Solution

La solution est disponible dans le repository, branche `solution` ; la classe se nomme `FirstNaiveProcedural.java`

### TP 0.2 - Amélioration du programme précédent

*Travaillez à partir du code de la solution du TP précédent. Assurez-vous d'avoir bien compris le fonctionnement du programme !*

Cette première approche présente plusieurs problèmes :

- pas optimisée (déclaration des variables dans le mauvais scope)
- beaucoup trop procédural
- peu sémantique (il faut 'décrypter' l'algorithme pour comprendre ce qui est fait)
- présence de 'magic numbers'
- mélange de code 'technique' (boucle, variables temporaires, conversion de type...) avec du code 'métier' (calcul
d'avoir global, calcul d'avoir total)

Il faut remanier le code pour le rendre plus élégant :

- déplacer les déclarations de variables aux bons endroits 
- renommez les variables pour leur donner un nom le plus parlant possible et le plus proche du besoin exprimé
- réorganisez le code de la fonction main() en plusieurs sections, dont la responsabilité de chacune est clairement identifiée
    1. récupération des paramètres 
    2. conversions des valeurs
    3. calculs
    4. affichage

- une fois ces parties de code isolées, créez autant de méthodes qu'il y a de parties et nommez les de la façon la plus 
explicite possible en déplacant le code dans ces fonctions
- remplacez les 'magic numbers' par des *constantes* correctement nommées

Executez votre programme en ligne de commande en lui passant des paramètres et vérifiez visuellement que la sortie
console est conforme à ce qui est demandé. Elle doit être bien entendu identique à la précédente.

**Utilisez les raccourcis clavier de votre éditeur de code** !

Les raccourcis clavier vous aident à réaliser le plus efficacement possible une très grande majorité des tâches
de développements. Bannissez autant que possible le copié/collé manuel et le chercher / remplacer ! Le gain de productivité
est très substantiel.

La maitrise de l'IDE fait partie des compétences de tout développeur sérieux et doit être assimilée au même titre
que la connaissance du langage de programmation.

Référez vous à l'aide de votre éditeur de code pour trouver les raccourcis utiles.

#### Pour vous aider / Rappels :

- Déclaration de constante en Java (ex. avec un entier) : `public static final int MAXIMUM = 1`

#### Solution

La solution est disponible dans le repository et est taggée "FirstProcedural".

### Etat actuel de notre développement

Les TPs précédent vous on permis de créer un programme java fonctionellement simple, mais permettant déjà de mettre
en oeuvre des concepts importants de programmation professionelle :

- **sémantique**
- découpage d'un algorithme en fonction des **responsabilités** de chaque partie de celui-ci
- création de fonctions **privées à usage interne**

Notez que *malgrès tout ces efforts*, le programme n'est pas forcément simple à comprendre... 
L'absence d'utilisation
des concepts objets fait pour l'instant cruellement défaut, et la démultiplication de tableaux (à valeur sémantique
intrinsèque très faible) oblige a faire un effort de décryptage mental important (et inutile).

Nous allons voir dans le tp suivant comment les mécanismes objets vont nous permettre de résoudre une partie de ces problèmes.

### TP 0.3 - Intégration des concepts objets : classe, propriétés et constructeur

*Travaillez à partir du code de la solution du TP précédent. Assurez-vous d'avoir bien compris le fonctionnement du programme !*

Afin d'améliorer notre programme, nous allons introduire des classes pour mieux structurer l'information
et se débarasser le plus possible des tableaux à la sémantique trop faible.

Notre programme manipule des clients et des comptes. Nous allons donc créer :

- une classe `Client`
- une classe `Compte`

Copiez la classe solution du TP précédent, et renommez-la en `FirstProceduralWithClasses`, puis complétez le code
comme illustré ci-après :

    public class FirstProceduralWithClasses {

        // code précédemment écrit omis...

        private static class Client {
            // Complétez ici pour ajouter une propriété 'nom' de type 'String'
            // Complétez ici pour ajouter un constructeur prenant la valeur initiale de nom en paramètre
        }

        // Sur le même principe que la classe Client, ajoutez ici une classe 'Compte'
        // avec une propriété 'balance' (choisissez le type soigneusement)
    }

Dans l'exemple ci-dessus, la classe `Client` est **privée**, elle n'est donc **visible qu'à l'intérieur de la classe englobante**.
Il est très important, au fur et à mesure de l'élaboration des programmes, de travailler avec une visibilité et une portée initiale 
la plus restrictive, pour ensuite l'augmenter au besoin.

Transformez ensuite le code pour ne plus utiliser des tableaux de types primitifs, mais des tableaux
contenant des instances des types `Client` et `Compte` :

- `String[] clientsNames` devient `Client[] clients`
- `float[][] clientsBalances` devient `Compte[][] clientsAccounts`

Executez votre programme en ligne de commande en lui passant des paramètres et vérifiez visuellement que la sortie
console est conforme à ce qui est demandé et que **votre remaniement à base de classes n'a pas impacté le fonctionnement
du programme observé précédemment**.

#### Solution

La solution est disponible dans le repository et est taggée "FirstProceduralWithClasses".

### TP 0.4 - Intégration des concepts objets : encapsulation, responsabilités des classes

*Travaillez à partir du code de la solution du TP précédent. Assurez-vous d'avoir bien compris le fonctionnement du programme !*

Plutôt que de continuer à manipuler plusieurs tableaux décorellés, nous allons rattacher le tableau de comptes
directement à la classe Client. Nous allons en profiter pour ajouter des accesseurs (get et set) sur les propriétés
de la classe `Client` et de la classe `Compte`. En déplaçant ainsi le tableau de comptes, nous allons pouvoir redistribuer
certaines responsabilités, en rapport avec ces comptes, à la classe `Client`.

Copiez la classe solution du TP précédent et renommez la en `FirstProceduralWithClassesAndEncapsulation`, puis :

- Modifiez la visibilité des attributes pour les rendres privés et rajoutez des getters / setters .
 Modifiez le code pour les utiliser
- Supprimez la variable locale `Compte[][] clientsAccounts` et transformez la de façon a en faire un attribut de 
la classe Client (attention, le tableau devient unidimensionnel !). Modifiez le code en conséquence.
- Le Client étant maintenant porteur du tableau de comptes, il est a même de calculer lui même la balance totale
de ses comptes. Ajoutez dans la classe Client une méthode `public float calculerAvoirGlobal()` qui va renvoyer
cette valeur, supprimer la variable `float[] totalBalanceByClient`maintenant inutile et modifiez le code en conséquence. 

Il s'agit d'un refactoring important qui impacte beaucoup plus le code que les précédents. Mais vous constaterez
qu'après cette opération, le code dans sa globalité est bien plus clair et lisible que précédemment.

Executez votre programme en ligne de commande en lui passant des paramètres et vérifiez visuellement que la sortie
console est conforme à ce qui est demandé et que **votre derniez remaniement important n'a pas impacté le fonctionnement
du programme observé précédemment**.

#### Solution

La solution est disponible dans le repository et est taggée "FirstProceduralWithClassesAndEncapsulation".

### TP Bankonet - Création des concepts de base

A partir de ce point, nous allons construire au fil des TP une petite application, **"Bankonet"** mettant en jeu des concepts du domaine bancaire.

Au fur et à mesure, nous raffinerons le modèle objet pour introduire les concepts objets / pratiques avancés.

#### Initialisation de l'application Bankonet

- Créez un nouveau projet (de type Maven, "java application")
- Vous devez créer un package java dont le nom complet est `com.sqli.bankonet`
    - Il s'agit du package *de base* de votre application
    - Rappellez vous que l'usage est de reprendre l'alias internet de votre société, et d'ajouter le nom du projet
    - Il ne faut pas créer de classes directement dans ce package. Il faut créer des sous packages pour organiser les classes de façon pertinente
- Le package racine des applications Java est habituellement subdivisé en trois packages
    - `model` (ou `domain`), qui contient les classes issues du domaine métier (Client, Compte, ...)
    - `service`, qui va contenir des classes offrant des services de plus haut niveau, autour des objets du domaine
    - `dao` (Data Access Objects), qui va contenir des classes dont le rôle et d'aller chercher de l'information stockées quelque part (base de donnée, web-services...)
- Pour l'instant, nous allons nous contenter de créer 
    - le sous-package `model`, ce qui nous donne un package `com.sqli.bankonet.model`
    - un sous sous-package `test`, dans `model`, ce qui nous donne un package `com.sqli.bankonet.model.test`
- Dans ces packages, créer conformément au diagramme les classes Java suivantes :
    - `CompteCourant`, `Client` et `ClientEtComptesTest`
    - les attributs doivents être privés (symbole `-` sur le diagramme)
    - ajoutez des getter / setter sur les attributs pour lesquels celà a du sens (non représentés sur le diagramme)
    - créez les constructeur qui vous semblent *pertinents* pour ces deux classes (non représentés sur le diagramme)
    - créeé et codez les fonctions (publiques, symbole `+`) présentées sur le diagramme en vous basant sur leur nom

    
    ![Classes à créer](https://bitbucket.org/snichele/training-java/raw/9cd0d0a76d246d5614fa5c50742c900ced548d0f/class%20diagram.jpg)

- 'Testez' votre code en écrivant une méthode main dans la classe `ClientEtComptesTest` qui :
    - Crée deux clients, avec chacun un nombre variable de comptes
    - Affiche le solde global de chacun de ces clients

