
# Projet

## Gestion d'un centre de formation

### Votre objectif : développer une application de gestion / suivi des sesssion des cours d'un centre de formation.

Cette application sera développée en plusieurs étapes :

- première session projet : développement de l'application en JEE 'de base', sans base de données
- deuxième session projet : intégration des frameworks d'entreprise dans l'application et gestion de la persistence.

### Le besoin :

L'application doit permettre de gérer une liste des 'cours' proposés par le centre de formation. Les cours sont organisés en 'catégories'.

Voici un exemple visuel illustrant ce que peut-être une fiche de cours dans la réalité :

http://www.sqli-institut.com/formation/fiche_pdt~ref~J-EJB.htm

Les informations constituants un cours sont :

- titre (String)
- objectif (String)
- programme (Programme)
- durée (int)
- coût par personne (int)
- public visé (String)
- pré-requis (String)

Le 'programme' se compose de un ou de plusieurs 'sujets'

Un 'sujet' de compose de 'sous-sujets' et d'un titre (String)

Un 'sous-sujet' est définit par son titre (String)

On doit pouvoir gérer une liste de 'sessions de cours'.

Une 'session' est créée pour une date donnée et concerne un cours donné.

On doit pouvoir affecter des 'stagiaires' à la session.

Un 'stagiaire' est définit par :

- nom (String)
- prenom (String)
- nom de la société du stagiaire (String)
- contact email (String)

Un administrateur à accès à une page de gestion des cours et des sessions.

Les visiteurs du site on le doit de voir la liste des cours dispensés. Ils peuvent cliquer sur un bouton pour demander à être inscrits à une prochaine session.

### Projet 1 : développement du modèle métier et d'une interface web simple.

Créer un projet maven multi-modules 'training-center-manager'. Créez deux sous modules,
'training-center-manager-core' et 'training-center-manager-webapp'.

Dans 'training-center-manager-core', développer le coeur de l'application :

- architecture en 'couche' : package `service`, package `dao` et package `model`
- tests unitaires

Les classes DAO renverront bien sûr pour l'instant des objets codés en dur (cf. exemple du cours développé ensemble).

Dans 'training-center-manager-webapp', développer l'interface web.

#### Démarche

Il peut-être pertinent de répartir le travail entre membres du groupe pour paralléliser au maximum les taches.

1. Commencez par coder les classes métiers (Cours, Sujet, Stagiaire...)
2. En parallèle, commencez à développer les écrans web. 

	- Les controlleurs doivent appeller les interfaces de la couche service
	- Il est tout à fait pertinent de 'construire' la couche service en partant des écrans (on crée les fonctions utiles au fur et à mesure que les controlleurs en ont besoin)
	- Attention de bien utiliser les classes du modèle au niveau de la couche service et ne pas inventer des nouveau concepts !


Les écrans doivent permettre de :

- gérer la liste des cours (création, modification, suppression, désactivation)

	- la suppression d'un cours n'est possible que si aucune session n'est référencée pour ce cour
	- la désactivation d'un cours permet de le retirer de la liste des cours dispensés

- gérer une liste de stagiaires (création, modification, suppression)
- gérer une liste de sessions (création, modification, suppression)

