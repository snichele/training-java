
# Exercices de révision

Les exercices suivants mettent en oeuvre tous les concepts étudiés précédemment.

Choisissez l'un des exercice et suivez les instructions associées :

- le premier exercice est assez simple mais permet de mettre en pratique ce que nous avons vu sans difficultés supplémentaires
- le deuxième est plus ardu car mettant en jeu des 'rêgles fonctionelles' plus complexes.

## Ex 1 : Site de e-commerce (facile)

### Exigences : Développer un site de e-commerce.

Un ami distributeur de produits régionaux souhaite que vous développiez un site internet pour lui permettre de vendre ses produits via internet.

Ses produits sont classés en catégories :

- vins
- jus de fruit
- fruits et légumes
- bibelots manufacturés

Chaque produit possède un prix :

- vin : à la bouteille
- jus de fruit : au litre
- fruits et légumes : au kilo
- bibelots manufacturés : à l'unité

Le client visite le site, choisit les produits qui l'intéresse et met dans son panier les quantités qu'il souhaite acquerir.

Il peut ensuite valider son panier dont il connait le montant exact hors frais de port.

### Votre objectif

N'ayant pas développé de site internet auparavent, vous comptez suivre une formation JEE prochainement. En attendant, vous pouvez cependant déjà coder les concepts métier de votre programme !

Créer un projet maven multi-module que vous appellerez `e-boutique-du-terroir`. Créez un sous module `ebdt-model` qui contiendra les classes métiers permettant de répondre au besoins exprimés ci-dessus. Créez des tests unitaires pour vous assurer que votre modele fonctionne.

## Ex 2 : Jeu de cartes

Enoncé disponible sur :
https://bitbucket.org/snichele/java-example-emergent-design-cardgame-project/overview

